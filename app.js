var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var app = express();
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static("views"));
app.use(express.static("images"));
app.use(express.static("css"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
mongoose.Promise = global.Promise;

const Developer = require('./models/developer');
const Task = require('./models/task');
 
const url = 'mongodb://localhost:27017/momatDB';
//Connecting MongoDB to the client
mongoose.connect(url, function(err){
    if(err){
        console.log('Error in connection');
    }
    else{
        console.log('Successfully connected'); 
    }
});


app.get('/', function(req, res){
    console.log('Thank you for the request');
    res.render('index.html');
});

app.get('/newtask', function(req, res){
    res.render('newtask.html');
});

app.get('/listtasks', function(req, res){
    Task.find({}, function(err, docs){
        if(err) throw err;
        res.render('listtasks.html', {tasks: docs});
    });
    
});

app.get('/updatetask', function(req, res){
    res.render("updatetask.html");
});

app.get('/deletetask', function(req, res){
    res.render("deletetask.html");
});

app.get('/deleteall', function(req, res){
    res.render("deleteall.html");
});

app.get('/newdeveloper', function(req, res){
    res.render('newdeveloper.html');
});

app.get('/listdevelopers', function(req, res){
    Developer.find({}, function(err, docs){
        if(err) throw err;
        res.render('listdevelopers.html', {developers: docs});
    });
});
app.post('/newtask', function(req, res){
    let tDetails = req.body;
    let id = Math.round(Math.random()*100);
    let task = new Task({
        _id: new mongoose.Types.ObjectId(),
        taskId: id,
        name: tDetails.taskname,
        assign: mongoose.Types.ObjectId(tDetails.taskassign),
        dueDate: new Date(tDetails.taskdue),
        status: tDetails.taskstat,
        description: tDetails.taskdesc
    });
    task.save(function(err){
        if(err) throw err;
        console.log('task successfully added to my DB');
    });
    res.render('index.html');
});

app.post('/newdeveloper', function(req, res){
    
    var developer = new Developer({
     _id: new mongoose.Types.ObjectId(),
     name: {
         firstName: req.body.firstName,
         lastName: req.body.lastName
     },
     level: req.body.level,
     address: {
         state: req.body.state,
         suburb: req.body.suburb,
         street: req.body.street,
         unit: req.body.unit
     }
 
    });
     developer.save(function(err){
         if(err) throw err;
         console.log('Developer successfully added to mydb');
     });
     res.redirect('/listdevelopers');  
});

app.post('/updatetask', function(req, res){
    let tDet = req.body;
    let filter = {taskId: parseInt(tDet.tid)};
    let update = {$set: {status: tDet.newstat}};
    Task.updateOne(filter, update, function(err, docs){
        if(err) throw err;
        console.log('task successfully updated');
    });
    res.redirect("/listtasks");
});

app.post('/deletetask', function(req, res){
    let tId = req.body;
    let filter = {taskId: parseInt(tId.delid)};
    Task.deleteOne(filter, function(err, docs){
        if(err) throw err;
        console.log('task successfully deleted');
    });
    res.redirect("/listtasks");
});

app.post('/deleteall', function(req, res){
    let tStat = req.body;
    let filter = {taskStat:tStat.delstat};
    col.deleteMany(filter, function(err, docs){
        if(err) throw err;
        console.log('All tasks successfully deleted');
    });
    res.redirect("/listtasks");
});

app.listen(8080);