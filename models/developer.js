var mongoose = require('mongoose');
var developerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        firstName: {
            type: String,
            required: true
        },
        lastName: String
    },
    level: String,
    address: {
        state: String,
        suburb: String,
        street: String,
        unit: String

    },
    level: String,
    address: {
        state: String,
        suburb: String,
        street: String,
        unit: String
    }

});
module.exports = mongoose.model('Developer', developerSchema);