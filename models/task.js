var mongoose = require('mongoose');
var taskSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    taskId: {
        type: Number
    },
    name: {
        type: String,
        required: true
    },
    assign: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Developer'
    },
    dueDate: {
        type: Date,
        //default: Date.now 
    },
    status: {
        type: String,
        required: true
    },
    description: String 
});
module.exports = mongoose.model('Task', taskSchema);